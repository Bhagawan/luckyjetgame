package com.example.luckyjetgame.api

import com.example.luckyjetgame.data.LuckyJetSplashResponse
import com.example.luckyjetgame.data.Record
import com.example.luckyjetgame.data.static.UrlRecords
import com.example.luckyjetgame.data.static.UrlSplash
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface LuckyJetServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<LuckyJetSplashResponse>

    @GET(UrlRecords)
    suspend fun getRecords() : Response<List<Record>>

    companion object {
        fun create() : LuckyJetServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(LuckyJetServerClient::class.java)
        }
    }
}