package com.example.luckyjetgame.data

import android.media.MediaPlayer

data class SoundWithState(val player: MediaPlayer,var isPrepared: Boolean = false)
