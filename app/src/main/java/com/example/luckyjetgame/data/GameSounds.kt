package com.example.luckyjetgame.data

import com.example.luckyjetgame.data.static.UrlAssetSound_explosion
import com.example.luckyjetgame.data.static.UrlAssetSound_pick

enum class GameSounds(val url: String) {
    EXPLOSION(UrlAssetSound_explosion), PICK(UrlAssetSound_pick)
}