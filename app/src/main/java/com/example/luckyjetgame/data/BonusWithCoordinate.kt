package com.example.luckyjetgame.data

data class BonusWithCoordinate(val bonus: GameBonuses, val coordinate: Coordinate)
