package com.example.luckyjetgame.data

enum class GameBonuses {
    ASTEROID, BOMB, CLOUD, X50, X100
}