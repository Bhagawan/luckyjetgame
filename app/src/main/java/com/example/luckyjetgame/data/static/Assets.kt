package com.example.luckyjetgame.data.static

import android.content.Context
import android.graphics.Bitmap
import android.media.MediaPlayer
import com.example.luckyjetgame.api.ImageDownloader
import com.example.luckyjetgame.data.GameSounds
import com.example.luckyjetgame.data.SoundWithState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlin.coroutines.EmptyCoroutineContext

object Assets {

    var player_1: Bitmap? = null
    var player_2: Bitmap? = null
    var player_3: Bitmap? = null
    var player_4: Bitmap? = null
    var x50: Bitmap? = null
    var x100: Bitmap? = null
    var asteroid: Bitmap? = null
    var bomb: Bitmap? = null
    var cloud: Bitmap? = null
    var back: Bitmap? = null

    val sounds = GameSounds.values().associateWith {
        SoundWithState(MediaPlayer()).apply {
            player.setDataSource(it.url)
            player.prepareAsync()
            player.setVolume(10.0f, 10.0f)
            player.setOnPreparedListener {
                isPrepared = true
            }
        }
    }


    val loaded : Boolean
    get() = player_1 != null
            && player_2 != null
            && player_3 != null
            && player_4 != null
            && x50 != null
            && x100 != null
            && asteroid != null
            && bomb != null
            && cloud != null
            && back != null

    fun loadAssets(context: Context, onAssetsLoaded : () -> Unit, onError : (error: Throwable) -> Unit) {
        val scope = CoroutineScope(EmptyCoroutineContext)
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_player_1,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                player_1 = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_player_2,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                player_2 = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_player_3,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                player_3 = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_player_4,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                player_4 = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_x50,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                x50 = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_x100,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                x100 = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_asteroid,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                asteroid = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_bomb,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                bomb = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlAsset_cloud,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                cloud = it
                if(loaded) onAssetsLoaded()
            })
        ImageDownloader.urlToBitmap(context, scope, UrlBack,
            onError = {
                scope.cancel()
                onError(it)
            },
            onSuccess = {
                back = it
                if(loaded) onAssetsLoaded()
            })

    }

    fun release() {
        sounds.forEach {
            it.value.player.stop()
            it.value.player.release()
        }
    }
}