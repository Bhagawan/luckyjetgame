package com.example.luckyjetgame.data

import androidx.annotation.Keep

@Keep
data class LuckyJetSplashResponse(val url : String)