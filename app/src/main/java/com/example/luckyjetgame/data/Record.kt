package com.example.luckyjetgame.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Record(@SerializedName("name") val name: String,
                  @SerializedName("score") val score: Int)
