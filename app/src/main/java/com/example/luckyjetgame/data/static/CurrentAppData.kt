package com.example.luckyjetgame.data.static

object CurrentAppData {
    var url = ""
    var yourName = "Player"
    var selectedPlayer = 0
    var money = 0
    var difficulty = 0
    var sounds = true
}