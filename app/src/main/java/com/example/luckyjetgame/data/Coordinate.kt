package com.example.luckyjetgame.data

data class Coordinate(var x: Int, var y: Int)
