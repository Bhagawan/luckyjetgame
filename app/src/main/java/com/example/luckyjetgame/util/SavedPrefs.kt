package com.example.luckyjetgame.util

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("LuckyJet", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        fun saveName(context: Context, name: String) = context
            .getSharedPreferences("LuckyJet", Context.MODE_PRIVATE)
            .edit()
            .putString("name", name)
            .apply()

        fun getName(context: Context): String = context.getSharedPreferences("LuckyJet", Context.MODE_PRIVATE).getString("name", "Player") ?: "Player"


        fun saveSoundSetting(context: Context, sound: Boolean) = context
            .getSharedPreferences("LuckyJet", Context.MODE_PRIVATE)
            .edit()
            .putBoolean("sound", sound)
            .apply()

        fun getSoundSetting(context: Context): Boolean = context.getSharedPreferences("LuckyJet", Context.MODE_PRIVATE).getBoolean("sound", true)

        fun saveDifficulty(context: Context, level: Int) = context
            .getSharedPreferences("LuckyJet", Context.MODE_PRIVATE)
            .edit()
            .putInt("difficulty", level)
            .apply()

        fun getDifficulty(context: Context): Int = context.getSharedPreferences("LuckyJet", Context.MODE_PRIVATE).getInt("difficulty", 0)

        fun saveMoney(context: Context, amount: Int) = context
            .getSharedPreferences("LuckyJet", Context.MODE_PRIVATE)
            .edit()
            .putInt("money", amount)
            .apply()

        fun getMoney(context: Context): Int = context.getSharedPreferences("LuckyJet", Context.MODE_PRIVATE).getInt("money", 0)

        fun saveSelectedSkin(context: Context, skin: Int) = context
            .getSharedPreferences("LuckyJet", Context.MODE_PRIVATE)
            .edit()
            .putInt("selectedSkin", skin)
            .apply()

        fun getSelectedSkin(context: Context): Int = context.getSharedPreferences("LuckyJet", Context.MODE_PRIVATE).getInt("selectedSkin", 3)

        fun saveSkins(context: Context, skins: List<Boolean>) = context.getSharedPreferences("LuckyJet", Context.MODE_PRIVATE)
                .edit()
                .putString("skins",Gson().toJson(skins))
                .apply()

        fun getSkins(context: Context): List<Boolean> = Gson().fromJson<List<Boolean>>(context.getSharedPreferences("LuckyJet", Context.MODE_PRIVATE).getString("skins", "[]")?: "[]").ifEmpty { listOf(false, false, false, true) }

        inline fun <reified T> Gson.fromJson(json: String) = fromJson<T>(json, object: TypeToken<T>() {}.type)
    }
}