package com.example.luckyjetgame.util.navigation

import android.webkit.WebView
import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.luckyjetgame.data.static.CurrentAppData
import com.example.luckyjetgame.ui.screens.*
import com.example.luckyjetgame.ui.screens.gameScreen.GameScreen
import com.example.luckyjetgame.ui.screens.scoresScreen.ScoresScreen
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: WebView, fixateScreen:  () -> Unit, reloadAssets: () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            BackHandler(true) { exitProcess(0) }
            SplashScreen()
        }
        composable(Screens.NETWORK_ERROR_SCREEN.label) {
            BackHandler(true) { exitProcess(0) }
            NetworkErrorScreen(reloadAssets)
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.canGoBack()) webView.goBack()
                else exitProcess(0)
            }
            WebViewScreen(webView, remember { CurrentAppData.url })
        }
        composable(Screens.MENU_SCREEN.label) {
            BackHandler(true) {}
            MenuScreen()
        }
        composable(Screens.SETTINGS_SCREEN.label) {
            SettingsScreen(onBackClick = { Navigator.navigateTo(Screens.MENU_SCREEN) })
        }
        composable(Screens.RULES_SCREEN.label) {
            RulesScreen(onBackClick = { Navigator.navigateTo(Screens.MENU_SCREEN) })
        }
        composable(Screens.STORE_SCREEN.label) {
            StoreScreen(onBackClick = { Navigator.navigateTo(Screens.MENU_SCREEN) })
        }
        composable(Screens.SCORES_SCREEN.label) {
            ScoresScreen(onBackClick = { Navigator.navigateTo(Screens.MENU_SCREEN) })
        }
        composable(Screens.GAME_SCREEN.label) {
            GameScreen()
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    if(currentScreen != Screens.WEB_VIEW) fixateScreen()
    navController.navigate(currentScreen.label)

    navController.enableOnBackPressed(true)
}