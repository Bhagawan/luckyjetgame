package com.example.luckyjetgame

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.FrameLayout
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.rememberNavController
import com.example.luckyjetgame.data.static.Assets
import com.example.luckyjetgame.data.static.CurrentAppData
import com.example.luckyjetgame.ui.screens.Screens
import com.example.luckyjetgame.ui.theme.LuckyJetGameTheme
import com.example.luckyjetgame.util.SavedPrefs
import com.example.luckyjetgame.util.navigation.NavigationComponent
import com.example.luckyjetgame.util.navigation.Navigator

class MainActivity : ComponentActivity() {
    private lateinit var filePathCallback: ValueCallback<Array<Uri>>
    private lateinit var viewModel :LuckyJetMainViewModel

    var mLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if(it.resultCode == RESULT_OK) {
            it.data?.let { intent ->
                intent.data?.let { uri ->
                    filePathCallback.onReceiveValue(arrayOf(uri))
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this)[LuckyJetMainViewModel::class.java]
        if(savedInstanceState == null) viewModel.init((getSystemService(TELEPHONY_SERVICE) as TelephonyManager).simCountryIso, SavedPrefs.getId(this))
        Assets.loadAssets(this, onError = {
        }, onAssetsLoaded = viewModel::assetsLoaded)
        CurrentAppData.money = SavedPrefs.getMoney(this)
        CurrentAppData.difficulty = SavedPrefs.getDifficulty(this)
        CurrentAppData.sounds = SavedPrefs.getSoundSetting(this)
        CurrentAppData.yourName = SavedPrefs.getName(this)
        CurrentAppData.selectedPlayer = SavedPrefs.getSelectedSkin(this)
        setContent {
            val webView = remember { createWebView() }
            val navController = rememberNavController()
            LuckyJetGameTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavigationComponent(navController, webView, ::fixScreen , ::reloadAssets)
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        SavedPrefs.saveMoney(this, CurrentAppData.money)
        SavedPrefs.saveDifficulty(this, CurrentAppData.difficulty)
        SavedPrefs.saveName(this, CurrentAppData.yourName)
        SavedPrefs.saveSelectedSkin(this, CurrentAppData.selectedPlayer)
    }

    override fun onDestroy() {
        super.onDestroy()
        Assets.release()
        SavedPrefs.saveMoney(this, CurrentAppData.money)
        SavedPrefs.saveDifficulty(this, CurrentAppData.difficulty)
        SavedPrefs.saveName(this, CurrentAppData.yourName)
        SavedPrefs.saveSelectedSkin(this, CurrentAppData.selectedPlayer)
    }

    private fun reloadAssets() {
        Navigator.navigateTo(Screens.SPLASH_SCREEN)
        Assets.loadAssets(this, onError = {
            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
        }, onAssetsLoaded = viewModel::assetsLoaded)
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private  fun fixScreen() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun createWebView() : WebView {
        return WebView(this).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            CookieManager.getInstance().setAcceptThirdPartyCookies(this, true)
            webViewClient = WebViewClient()
            webChromeClient = object: WebChromeClient() {
                private var h = 0
                private var uiVisibility = 0
                private var d: View? = null
                private var a: CustomViewCallback? = null
                override fun getDefaultVideoPoster(): Bitmap? = if(d == null) null else BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_video)
                override fun onHideCustomView() {
                    (window.decorView as FrameLayout).removeView(d)
                    d = null
                    window.decorView.systemUiVisibility = uiVisibility
                    requestedOrientation = h
                    a!!.onCustomViewHidden()
                    a = null
                }

                override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                    if(d != null) {
                        onHideCustomView()
                        return
                    }
                    d = view
                    uiVisibility = window.decorView.systemUiVisibility
                    h = requestedOrientation
                    a = callback
                    (window.decorView as FrameLayout).addView(d, FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                    WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                        controller.hide(WindowInsetsCompat.Type.systemBars())
                        controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    }
                }

                override fun onShowFileChooser(view: WebView, filePath: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
                    filePathCallback = filePath
                    val i = Intent(Intent.ACTION_GET_CONTENT)
                    i.addCategory(Intent.CATEGORY_OPENABLE)
                    i.type = "image/*"
                    i.putExtra(Intent.EXTRA_TITLE, "File Chooser")
                    mLauncher.launch(i)
                    return true
                }
            }
            settings.domStorageEnabled = true
            settings.allowContentAccess = true
            settings.allowFileAccess = true
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            settings.loadWithOverviewMode = true
            settings.useWideViewPort = true
            settings.javaScriptEnabled = true
            settings.userAgentString = settings.userAgentString.replace("; wv", "")
        }
    }
}