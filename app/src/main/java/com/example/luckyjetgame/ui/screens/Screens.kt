package com.example.luckyjetgame.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    WEB_VIEW("web_view"),
    NETWORK_ERROR_SCREEN("network_error"),
    MENU_SCREEN("menu"),
    SETTINGS_SCREEN("settings"),
    RULES_SCREEN("rules"),
    STORE_SCREEN("store"),
    SCORES_SCREEN("scores"),
    GAME_SCREEN("game")
}