package com.example.luckyjetgame.ui.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import com.example.luckyjetgame.R
import com.example.luckyjetgame.data.BonusWithCoordinate
import com.example.luckyjetgame.data.Coordinate
import com.example.luckyjetgame.data.GameBonuses
import com.example.luckyjetgame.data.GameSounds
import com.example.luckyjetgame.data.static.Assets
import com.example.luckyjetgame.data.static.CurrentAppData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.random.Random

class GameView (context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var speed = 1
    private var playerWidth = 1
    private var playerHeight = 1
    private var playerCoordinate = Coordinate(0,0)
    private var objectSize = 1
    private val difficulty = CurrentAppData.difficulty

    private val nameTopLine = context.resources.getString(R.string.name_top)
    private val nameBottomLine = context.resources.getString(R.string.name_bottom)
    private val score = context.resources.getString(R.string.game_score)
    private val bonuses = ArrayList<BonusWithCoordinate>()

    private var scoreIncrementTimer = 0
    private var spawnTimer = 0

    private val playerBitmap = when(CurrentAppData.selectedPlayer) {
        3 -> Assets.player_1
        2 -> Assets.player_2
        1 -> Assets.player_3
        else -> Assets.player_4
    } ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val asteroidBitmap = Assets.asteroid ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val bombBitmap = Assets.bomb ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val cloudBitmap = Assets.cloud ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val x50Bitmap = Assets.x50 ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val x100Bitmap = Assets.x100 ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val backBitmap = Assets.back ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)

    private var mInterface: GameInterface? = null

    companion object {
        const val STATE_GAME = 0
        const val STATE_PAUSE = 1
    }

    private var state = 0

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            playerWidth = mWidth / 4
            playerHeight = playerBitmap.height / playerBitmap.width * playerWidth
            playerCoordinate.x = playerWidth / 2
            playerCoordinate.y = mHeight / 2
            speed = mWidth / 120 * ( difficulty + 1)
            objectSize = mWidth / 4
        }
    }

    override fun onDraw(canvas: Canvas) {
        drawBackground(canvas)
        if(state == STATE_GAME) {
            updateScore()
            spawnBonus()
            moveBonuses()
        }
        drawBonuses(canvas)
        drawPlayer(canvas)
        drawUI(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE,
            MotionEvent.ACTION_UP -> {
                if(state == STATE_GAME) movePlayerTo(event.x, event.y)
                return true
            }
        }
        return false
    }

    //// Public

    fun setInterface(i: GameInterface) {
        mInterface = i
    }

    fun pause() {
        state = STATE_PAUSE
    }

    //// Private

    private fun drawBackground(c: Canvas) {
        c.drawBitmap(backBitmap, null, Rect(0, 0,mWidth, mHeight), Paint())
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.parseColor("#FFC99900")
        p.textSize = 70.0f
        p.isFakeBoldText = true
        p.textAlign = Paint.Align.CENTER
        c.drawText(nameBottomLine, mWidth / 4.0f, 140.0f, p)
        p.color = Color.WHITE
        c.drawText(nameTopLine, mWidth / 4.0f, 70.0f, p)
        c.drawText(score, mWidth / 4.0f * 3, 70.0f, p)
        c.drawText(CurrentAppData.money.toString(), mWidth / 4.0f * 3, 140.0f, p)
    }

    private fun drawBonuses(c: Canvas) {
        val p = Paint()
        for(bonus in bonuses) {
            val bitmap = when(bonus.bonus) {
                GameBonuses.ASTEROID -> asteroidBitmap
                GameBonuses.BOMB -> bombBitmap
                GameBonuses.CLOUD -> cloudBitmap
                GameBonuses.X50 -> x50Bitmap
                GameBonuses.X100 -> x100Bitmap
            }
            c.drawBitmap(bitmap, null,
                Rect(bonus.coordinate.x - objectSize / 2,
                    bonus.coordinate.y - objectSize / 2,
                    bonus.coordinate.x + objectSize / 2,
                    bonus.coordinate.y + objectSize / 2), p)
        }
    }

    private fun drawPlayer(c: Canvas) {
        val p = Paint()
        c.drawBitmap(playerBitmap, null,
            Rect(playerCoordinate.x - playerWidth / 2,
                playerCoordinate.y - playerHeight / 2,
                playerCoordinate.x + playerWidth / 2,
                playerCoordinate.y + playerHeight / 2), p)
    }

    private fun updateScore() {
        if(scoreIncrementTimer++ > 30) {
            CurrentAppData.money+=(difficulty + 1)
            scoreIncrementTimer = 0
        }
    }

    private fun moveBonuses() {
        for(bonus in bonuses) bonus.coordinate.x-=speed
        checkBonuses()
    }

    private fun checkBonuses() {
        var n = 0
        while(n < bonuses.size) {
            val coordinate = bonuses[n].coordinate
            if(coordinate.x < -objectSize / 2) {
                bonuses.removeAt(n)
                n--
            }else if((coordinate.x - playerCoordinate.x).absoluteValue < (playerWidth + objectSize) / 2 &&
                (coordinate.y - playerCoordinate.y).absoluteValue < (playerHeight + objectSize) / 2) {
                when(bonuses[n].bonus) {
                    GameBonuses.ASTEROID -> {
                        CurrentAppData.money = (CurrentAppData.money - 200).coerceAtLeast(0)
                        if(CurrentAppData.money == 0) {
                            state = STATE_PAUSE
                            mInterface?.playSound(GameSounds.EXPLOSION)
                            mInterface?.onDeath()
                        }
                    }
                    GameBonuses.BOMB -> {
                        CurrentAppData.money = 0
                        state = STATE_PAUSE
                        mInterface?.playSound(GameSounds.EXPLOSION)
                        mInterface?.onDeath()
                    }
                    GameBonuses.CLOUD -> {
                        CurrentAppData.money = (CurrentAppData.money - 100).coerceAtLeast(0)
                        if(CurrentAppData.money == 0) {
                            state = STATE_PAUSE
                            mInterface?.playSound(GameSounds.EXPLOSION)
                            mInterface?.onDeath()
                        }
                    }
                    GameBonuses.X50 -> {
                        mInterface?.playSound(GameSounds.PICK)
                        CurrentAppData.money+=50
                    }
                    GameBonuses.X100 -> {
                        mInterface?.playSound(GameSounds.PICK)
                        CurrentAppData.money+=100
                    }
                }
                bonuses.removeAt(n)
                n--
            }
            n++
        }
    }

    private fun spawnBonus() {
        var last = 0
        for(bonus in bonuses) if(last < bonus.coordinate.x) last = bonus.coordinate.x
        if(Random.nextInt(500) < spawnTimer++ && last <= mWidth - objectSize / 2) {
            spawnTimer = 0
            bonuses.add(BonusWithCoordinate(GameBonuses.values().random(), Coordinate(mWidth + objectSize / 2, Random.nextInt(objectSize / 2, mHeight - objectSize / 2))))
        }
    }

    private fun movePlayerTo(x: Float, y: Float) {
        playerCoordinate.x = x.toInt().coerceAtLeast(playerWidth / 2).coerceAtMost(mWidth - playerWidth / 2)
        playerCoordinate.y = y.toInt().coerceAtLeast(playerHeight / 2).coerceAtMost(mHeight - playerHeight / 2)
        checkBonuses()
    }

    interface GameInterface {
        fun playSound(sound: GameSounds)
        fun onDeath()
    }
}