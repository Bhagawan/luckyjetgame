package com.example.luckyjetgame.ui.screens.scoresScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.luckyjetgame.R
import com.example.luckyjetgame.data.Record
import com.example.luckyjetgame.data.static.Assets
import com.example.luckyjetgame.data.static.CurrentAppData
import com.example.luckyjetgame.data.static.UrlBack
import com.example.luckyjetgame.ui.theme.Gold

@Composable
fun ScoresScreen(onBackClick: () -> Unit) {
    Assets.back?.let {
        Image(it.asImageBitmap(), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    } ?: Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    val viewModel = viewModel(ScoresScreenViewModel::class.java)
    val you = Record(CurrentAppData.yourName, CurrentAppData.money)
    val scores = ArrayList(viewModel.scores).plus(you).sortedByDescending { it.score }
    val yourPlace = scores.indexOf(you)

    val scoresScrollState = rememberScrollState()
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(modifier = Modifier
            .fillMaxSize(0.9f), verticalArrangement = Arrangement.spacedBy(2.dp)) {
            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painterResource(id = R.drawable.ic_arrow_in_square), contentDescription = null, contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .size(50.dp)
                        .clickable(MutableInteractionSource(), null) { onBackClick() })
                Text(stringResource(id = R.string.btn_scores), color = Color.White, fontSize = 30.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, modifier = Modifier.weight(1.0f, true))
            }
            Spacer(modifier = Modifier.height(20.dp))
            Column(modifier = Modifier
                .weight(1.0f, true)
                .verticalScroll(scoresScrollState), verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
                scores.forEachIndexed { index, record ->
                    val color = if(index == yourPlace) Color.Green else Color.White
                    Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                        Text(index.toString(),
                            color = color, fontSize = 35.sp,
                            fontWeight = FontWeight.Bold,
                            textAlign = TextAlign.Center)
                        Text(record.name,
                            color = if(index == yourPlace) Color.Green else Gold, fontSize = 17.sp,
                            fontWeight = FontWeight.Bold,
                            textAlign = TextAlign.Center,
                            modifier = Modifier.weight(10.0f, true), textDecoration = TextDecoration.Underline)
                        Text(record.score.toString(),
                            color = color, fontSize = 17.sp,
                            fontWeight = FontWeight.Bold,
                            textAlign = TextAlign.Center,
                            modifier = Modifier.weight(6.0f, true), textDecoration = TextDecoration.Underline)
                    }
                }
            }
            Spacer(modifier = Modifier.height(20.dp))
        }
    }
}