package com.example.luckyjetgame.ui.composables

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.luckyjetgame.ui.theme.customFontFamily

@Composable
fun SettingsTextField(initial: String = "", modifier: Modifier = Modifier, onTextChanged: (String) -> Unit) {
    var text by remember { mutableStateOf(initial) }
    val focusManager = LocalFocusManager.current
    BasicTextField(
        value = text,
        onValueChange = {
            text = it
            onTextChanged(it)
        },
        singleLine = true,
        textStyle = TextStyle(fontSize = 20.sp, color = Color.White, textAlign = TextAlign.Center, textDecoration = TextDecoration.Underline, fontFamily = customFontFamily),
        modifier = modifier,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
        keyboardActions = KeyboardActions(onSearch = {
            focusManager.clearFocus()
        }),
        decorationBox = { innerTextField ->
            Box(modifier = Modifier.padding(1.dp), contentAlignment = Alignment.Center) {
                innerTextField()
            }
        }
    )
}