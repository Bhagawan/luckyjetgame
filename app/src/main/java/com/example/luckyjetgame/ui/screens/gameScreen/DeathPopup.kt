package com.example.luckyjetgame.ui.screens.gameScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.luckyjetgame.R
import com.example.luckyjetgame.ui.theme.Blue_dark
import com.example.luckyjetgame.ui.theme.Red

@Composable
fun DeathPopup() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Box(modifier = Modifier
            .fillMaxSize(0.5f)
            .background(Blue_dark, RoundedCornerShape(10.dp))
            .padding(10.dp)
            .border(width = 1.dp, color = Red, RoundedCornerShape(10.dp))
            .padding(10.dp), contentAlignment = Alignment.Center) {
            Text(stringResource(id = R.string.game_death), fontSize = 25.sp, textAlign = TextAlign.Center, color = Red )
        }
    }
}