package com.example.luckyjetgame.ui.screens

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.luckyjetgame.R
import com.example.luckyjetgame.data.static.Assets
import com.example.luckyjetgame.data.static.CurrentAppData
import com.example.luckyjetgame.data.static.UrlBack
import com.example.luckyjetgame.ui.theme.Gold
import com.example.luckyjetgame.util.SavedPrefs

@Composable
fun StoreScreen(onBackClick: () -> Unit) {
    Assets.back?.let {
        Image(it.asImageBitmap(), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    } ?: Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)

    val context = LocalContext.current
    val names = stringArrayResource(id = R.array.names)
    val prices = stringArrayResource(id = R.array.prices)
    var boughtSkins by remember { mutableStateOf(SavedPrefs.getSkins(context)) }

    var money by remember { mutableStateOf(CurrentAppData.money) }
    val storeScrollState = rememberScrollState()

    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(modifier = Modifier
            .fillMaxSize(0.9f), verticalArrangement = Arrangement.spacedBy(2.dp), horizontalAlignment = Alignment.CenterHorizontally) {
            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painterResource(id = R.drawable.ic_arrow_in_square), contentDescription = null, contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .size(50.dp)
                        .clickable(MutableInteractionSource(), null) { onBackClick() })
                Text(stringResource(id = R.string.btn_store), color = Color.White, fontSize = 40.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, modifier = Modifier.weight(1.0f, true))
            }
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                stringResource(id = R.string.store_money),
                color = Color.White,
                fontSize = 10.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center)
            Text(
                money.toString(),
                color = Color.White,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center)
            Spacer(modifier = Modifier.height(10.dp))
            Column(modifier = Modifier
                .weight(1.0f, true)
                .verticalScroll(storeScrollState), verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
                names.forEachIndexed { index, name ->
                    val skin = when(index) {
                        0 -> Assets.player_4?.asImageBitmap()
                        1 ->Assets.player_3?.asImageBitmap()
                        2 -> Assets.player_2?.asImageBitmap()
                        else -> Assets.player_1?.asImageBitmap()
                    }
                    val bought = if(boughtSkins.size > index) boughtSkins[index] else false
                    val price = if(prices.size > index){
                        try {
                           prices[index].toInt()
                        } catch (e: Exception) {
                            names.size - index * 5000
                        }
                    } else names.size - index * 5000
                    Text(
                        name,
                        color = Color.White,
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.align(Alignment.Start))
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .height(70.dp)
                        .clickable(MutableInteractionSource(), null) {
                            if (!bought && price <= money) {
                                money -= price
                                CurrentAppData.money -= price
                                SavedPrefs.saveMoney(context, money)
                                val array = ArrayList(boughtSkins)
                                if (boughtSkins.size > index) array[index] = true
                                else array.add(true)
                                boughtSkins = array.toList()
                                SavedPrefs.saveSkins(context, boughtSkins)
                            }
                        }, verticalAlignment = Alignment.CenterVertically) {
                        skin?.let{
                            Box(modifier = Modifier
                                .weight(1.0f, true), contentAlignment = Alignment.CenterStart) {
                                Image(it,
                                    contentDescription = null,
                                    modifier = Modifier
                                        .background(Gold, RoundedCornerShape(10.dp)),
                                    contentScale = ContentScale.FillHeight)
                            }
                        }
                        Spacer(modifier = Modifier.width(20.dp))
                        if(bought) Text(
                            stringResource(id = R.string.store_owned),
                            color = Color.Green,
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold,
                            textAlign = TextAlign.Start,
                            modifier = Modifier.weight(1.0f, true))
                        else Text(
                            price.toString(),
                            color = Color.White,
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold,
                            textAlign = TextAlign.Start,
                            modifier = Modifier.weight(1.0f, true))
                    }
                }
            }
        }
    }
}