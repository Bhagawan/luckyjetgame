package com.example.luckyjetgame.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.luckyjetgame.R
import com.example.luckyjetgame.data.static.Assets
import com.example.luckyjetgame.data.static.UrlBack

@Composable
fun RulesScreen(onBackClick: () -> Unit) {
    Assets.back?.let {
        Image(it.asImageBitmap(), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    } ?: Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(modifier = Modifier
            .fillMaxSize(0.9f)
            .padding(bottom = 50.dp), verticalArrangement = Arrangement.spacedBy(5.dp)) {
            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painterResource(id = R.drawable.ic_arrow_in_square), contentDescription = null, contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .size(50.dp)
                        .clickable(MutableInteractionSource(), null) { onBackClick() })
                Text(stringResource(id = R.string.btn_settings), color = Color.White, fontSize = 40.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, modifier = Modifier.weight(1.0f, true))
            }
            Row(modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f, true), verticalAlignment = Alignment.CenterVertically) {
                Text(stringResource(id = R.string.rules_asteroid),
                    color = Color.White,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.weight(1.0f, true))
                Assets.asteroid?.let {
                    Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                        Image(it.asImageBitmap(),
                            contentDescription = null,
                            modifier = Modifier.size(100.dp),
                            contentScale = ContentScale.Fit)
                    }
                }
            }
            Row(modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f, true), verticalAlignment = Alignment.CenterVertically) {
                Text(stringResource(id = R.string.rules_cloud),
                    color = Color.White,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.weight(1.0f, true))
                Assets.cloud?.let {
                    Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                        Image(it.asImageBitmap(),
                            contentDescription = null,
                            modifier = Modifier.size(100.dp),
                            contentScale = ContentScale.Fit)
                    }
                }
            }
            Row(modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f, true), verticalAlignment = Alignment.CenterVertically) {
                Assets.player_1?.let {
                    Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                        Image(it.asImageBitmap(),
                            contentDescription = null,
                            modifier = Modifier.size(100.dp),
                            contentScale = ContentScale.Fit)
                    }
                }
                Text(stringResource(id = R.string.rules_bomb),
                    color = Color.White,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.weight(1.0f, true))
                Assets.bomb?.let {
                    Box(modifier = Modifier.weight(2.0f, true), contentAlignment = Alignment.Center) {
                        Image(it.asImageBitmap(),
                            contentDescription = null,
                            modifier = Modifier.size(100.dp),
                            contentScale = ContentScale.Fit)
                    }
                }
            }
            Row(modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f, true), verticalAlignment = Alignment.CenterVertically) {
                Text(stringResource(id = R.string.rules_points),
                    color = Color.White,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.weight(1.0f, true))
                Assets.x100?.let {
                    Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                        Image(it.asImageBitmap(),
                            contentDescription = null,
                            modifier = Modifier.size(100.dp),
                            contentScale = ContentScale.Fit)
                    }
                }
            }
            Spacer(modifier = Modifier.weight(1.0f, true))
        }
    }
}