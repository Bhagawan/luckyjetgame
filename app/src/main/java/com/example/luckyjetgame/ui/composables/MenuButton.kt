package com.example.luckyjetgame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.luckyjetgame.ui.theme.Blue
import com.example.luckyjetgame.ui.theme.Purple80
import com.example.luckyjetgame.ui.theme.customFontFamily

@Composable
fun MenuButton(text: String, fontSize: TextUnit = 25.sp, modifier: Modifier = Modifier, onClick: () -> Unit) {
    BoxWithConstraints(modifier = modifier.clickable { onClick() }) {
        val brush = Brush.linearGradient(
            0.0f to Purple80,
            0.7f to Blue,
            start = Offset(0.0f, 0.0f),
            end = Offset(maxWidth.value, maxHeight.value)
        )
        Text(
            text,
            fontSize = fontSize,
            color = Color.White,
            fontWeight = FontWeight.Bold,
            style = TextStyle(shadow = Shadow(Color.White, offset = Offset(2.0f, 2.0f)), fontFamily = customFontFamily),
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
                .background(brush, RoundedCornerShape(20.dp))
                .padding(5.dp)
                .clip(RoundedCornerShape(20.dp)))
    }
}