package com.example.luckyjetgame.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Blue = Color(0xFF005594)
val Gold = Color(0xFFC99900)
val Blue_dark = Color(0xFF2C3149)
val Red = Color(0xFF700C0C)
