package com.example.luckyjetgame.ui.screens

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.luckyjetgame.R
import com.example.luckyjetgame.data.static.Assets
import com.example.luckyjetgame.data.static.CurrentAppData
import com.example.luckyjetgame.data.static.UrlBack
import com.example.luckyjetgame.ui.composables.SettingsOption
import com.example.luckyjetgame.ui.composables.SettingsTextField
import com.example.luckyjetgame.ui.theme.Gold
import com.example.luckyjetgame.util.SavedPrefs

@Composable
fun SettingsScreen(onBackClick: () -> Unit) {
    Assets.back?.let {
        Image(it.asImageBitmap(), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    } ?: Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)

    val context = LocalContext.current
    var name = remember { SavedPrefs.getName(context) }
    var sound = remember { SavedPrefs.getSoundSetting(context) }
    var difficulty = remember { SavedPrefs.getDifficulty(context) }
    val diffArray = stringArrayResource(id = R.array.difficulty).toList()

    val availableSkins = remember { SavedPrefs.getSkins(context).withIndex().filter { it.value } }
    var selectedSkin by remember { mutableStateOf( CurrentAppData.selectedPlayer) }

    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column(modifier = Modifier
            .fillMaxSize(0.9f)
            .padding(bottom = 50.dp), verticalArrangement = Arrangement.spacedBy(10.dp), horizontalAlignment = Alignment.Start) {
            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Image(painterResource(id = R.drawable.ic_arrow_in_square), contentDescription = null, contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .size(40.dp)
                        .clickable(MutableInteractionSource(), null) { onBackClick() })
                Text(stringResource(id = R.string.btn_settings), color = Color.White, fontSize = 40.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, modifier = Modifier.weight(1.0f, true))
            }
            Spacer(modifier = Modifier.weight(1.0f, true))
            Text(stringResource(id = R.string.settings_name), color = Color.White, fontSize = 20.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)
            SettingsTextField(initial = name, modifier = Modifier.fillMaxWidth(), onTextChanged = {
                SavedPrefs.saveName(context, it)
                name = it
            })
            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Text(stringResource(id = R.string.settings_sound), color = Color.White, fontSize = 20.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)
                SettingsOption(options = listOf("on", "off"), modifier = Modifier
                    .weight(1.0f, true)
                    .padding(start = 50.dp),
                    selected = if(sound) 0 else 1,
                    onOptionSelected = {
                        sound = it == 0
                        CurrentAppData.sounds = it == 0
                        SavedPrefs.saveSoundSetting(context, sound)
                    })
            }
            Text(stringResource(id = R.string.settings_difficulty), color = Color.White, fontSize = 20.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)
            Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                SettingsOption(options = diffArray,
                    selected = difficulty,
                    onOptionSelected = {
                        difficulty = it
                        CurrentAppData.difficulty = it
                        SavedPrefs.saveDifficulty(context, it)
                    })
            }
            LazyVerticalGrid(columns = GridCells.Adaptive(minSize = 100.dp), modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f, true)) {
                items(availableSkins) { skin ->
                    val bitmap = when(skin.index) {
                            0 -> Assets.player_4
                            1 -> Assets.player_3
                            2 -> Assets.player_2
                            else -> Assets.player_1
                        } ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
                    Image(bitmap.asImageBitmap(),
                        contentDescription = null,
                        modifier = Modifier
                            .width(100.dp)
                            .background(if(selectedSkin == skin.index) Gold else Color.Transparent, RoundedCornerShape(10.dp))
                            .align(Alignment.CenterHorizontally)
                            .clickable(MutableInteractionSource(), null) {
                                selectedSkin = skin.index
                                CurrentAppData.selectedPlayer = skin.index
                            },
                        contentScale = ContentScale.FillWidth)
                    }
                }
            }
    }

}