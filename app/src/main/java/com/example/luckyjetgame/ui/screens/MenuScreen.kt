package com.example.luckyjetgame.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.luckyjetgame.R
import com.example.luckyjetgame.data.static.UrlBack
import com.example.luckyjetgame.data.static.UrlLogo
import com.example.luckyjetgame.ui.composables.MenuButton
import com.example.luckyjetgame.ui.theme.Gold
import com.example.luckyjetgame.util.navigation.Navigator

@Composable
fun MenuScreen() {
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        val widthDp = maxWidth
        Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
        Column(modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = maxWidth / 5.0f), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(10.dp)) {
            Text(text = stringResource(id = R.string.name_top) + " " + stringResource(id = R.string.name_bottom) , fontSize = 40.sp, color = Gold, textAlign = TextAlign.Center, lineHeight = 45.sp, fontWeight = FontWeight.Bold)
            Image(rememberImagePainter(UrlLogo), contentDescription = null, modifier = Modifier.width(widthDp / 3), contentScale = ContentScale.FillWidth)
            MenuButton(text = stringResource(id = R.string.btn_play)) {
                Navigator.navigateTo(Screens.GAME_SCREEN)
            }
            MenuButton(text = stringResource(id = R.string.btn_rules)) {
               Navigator.navigateTo(Screens.RULES_SCREEN)
            }
            MenuButton(text = stringResource(id = R.string.btn_store)) {
                Navigator.navigateTo(Screens.STORE_SCREEN)
            }
            MenuButton(text = stringResource(id = R.string.btn_settings)) {
               Navigator.navigateTo(Screens.SETTINGS_SCREEN)
            }
            Spacer(modifier = Modifier.weight(1.0f, true))
            MenuButton(modifier = Modifier
                .width(widthDp / 4)
                .padding(bottom = 20.dp), text = stringResource(id = R.string.btn_scores), fontSize = 12.sp) {
                Navigator.navigateTo(Screens.SCORES_SCREEN)
            }
        }
    }
}