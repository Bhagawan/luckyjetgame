package com.example.luckyjetgame.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.luckyjetgame.data.GameSounds
import com.example.luckyjetgame.data.static.Assets
import com.example.luckyjetgame.data.static.CurrentAppData
import com.example.luckyjetgame.ui.screens.Screens
import com.example.luckyjetgame.util.navigation.Navigator
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class GameScreenViewModel: ViewModel() {
    private val _deathPopup = MutableStateFlow(false)
    val deathPopup = _deathPopup.asStateFlow()

    private var timer : Job? = null

    fun death() {
        if(!deathPopup.value) {
            CurrentAppData.money = 0
            _deathPopup.tryEmit(true)
            timer?.cancel()
            timer = viewModelScope.async {
                delay(2000)
                Navigator.navigateTo(Screens.MENU_SCREEN)
            }
        }
    }

    fun playSound(sound: GameSounds) {
        if(CurrentAppData.sounds) Assets.sounds[sound]?.let{
            if(it.isPrepared) {
                if(it.player.isPlaying) it.player.seekTo(0)
                it.player.start()
            }
        }
    }
}