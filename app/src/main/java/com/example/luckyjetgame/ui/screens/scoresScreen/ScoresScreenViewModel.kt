package com.example.luckyjetgame.ui.screens.scoresScreen

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.luckyjetgame.api.LuckyJetServerClient
import com.example.luckyjetgame.data.Record
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class ScoresScreenViewModel: ViewModel() {
    private var scoresRequest: Job? = null
    val scores = mutableStateListOf<Record>()

    init {
        if(scoresRequest?.isActive != true) {
            scoresRequest = viewModelScope.async {
                try {
                    val records = LuckyJetServerClient.create().getRecords()
                    viewModelScope.launch {
                        if(records.isSuccessful) {
                            if(records.body() != null) {
                                val s = records.body()!!
                                scores.clear()
                                scores.addAll(s.sortedBy { it.score })
                            }
                        }
                    }
                } catch(e: Exception) { }
            }
        }
    }
}