package com.example.luckyjetgame.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.luckyjetgame.R

@Composable
fun NetworkErrorScreen(onPress: () -> Unit) {
    Box(modifier = Modifier
        .fillMaxSize()
        .background(Color.White), contentAlignment = Alignment.Center) {
        Column(verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = stringResource(id = R.string.error_network), fontWeight = FontWeight.Bold, color = Color.White, fontSize = 20.sp)
            Image(
                painter = painterResource(id = R.drawable.ic_restart),
                contentDescription = stringResource(
                    id = R.string.desc_reload),
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .fillMaxWidth(0.3f)
                    .clickable { onPress() })
        }

    }
}