package com.example.luckyjetgame.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Yellow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import coil.compose.rememberImagePainter
import com.example.luckyjetgame.data.static.UrlBack
import com.example.luckyjetgame.data.static.UrlLogo

@Composable
fun SplashScreen() {
    Image(painter = rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Image(rememberImagePainter(UrlLogo), contentDescription = null)
        CircularProgressIndicator( color = Yellow, strokeWidth = Dp(5.0f),
            modifier = Modifier
                .padding(Dp(20.0f))
                .size(Dp(100.0f)))
    }
}