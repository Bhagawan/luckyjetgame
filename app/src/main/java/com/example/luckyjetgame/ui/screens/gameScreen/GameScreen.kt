package com.example.luckyjetgame.ui.screens.gameScreen

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.luckyjetgame.data.GameSounds
import com.example.luckyjetgame.data.static.CurrentAppData
import com.example.luckyjetgame.ui.views.GameView

@Composable
fun GameScreen() {
    val viewModel = viewModel(GameScreenViewModel::class.java)
    val context = LocalContext.current
    val game = remember {
        GameView(context)
    }
    val deathPopup by viewModel.deathPopup.collectAsState()
    AndroidView(factory = { game },
        modifier = Modifier.fillMaxSize(),
        update = {
            it.setInterface(object: GameView.GameInterface {
                override fun playSound(sound: GameSounds) {
                    viewModel.playSound(sound)
                }
                override fun onDeath() {
                    viewModel.death()
                }
            })
        })
    if(deathPopup) DeathPopup()
}