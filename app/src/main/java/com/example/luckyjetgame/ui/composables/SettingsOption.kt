package com.example.luckyjetgame.ui.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.luckyjetgame.ui.theme.Gold

@Composable
fun SettingsOption(options: List<String>, selected: Int = 0,fontSize: TextUnit = 20.sp,  modifier: Modifier = Modifier, onOptionSelected: (Int) -> Unit) {
    var selectedOption by remember { mutableStateOf(selected) }
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
        options.forEachIndexed { index, option ->
            Text(text = option,
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold,
                fontSize = fontSize,
                color = if(index == selectedOption) Gold else Color.White, modifier = Modifier.clickable(
                    MutableInteractionSource(), null) {
                    selectedOption = index
                    onOptionSelected(index)
                })
        }
    }
}